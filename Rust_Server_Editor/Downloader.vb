﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms

Module Downloader

    '================================ x64 patcher ======================
    Dim ini_patcher_x64 As New System.Threading.Thread(AddressOf ini_dl64)
    Dim client_patcher_x64 As New System.Threading.Thread(AddressOf client_dl64)
    Dim api_patcher_x64 As New System.Threading.Thread(AddressOf api_dl64)
    Dim finish_x64 As New System.Threading.Thread(AddressOf finished_x64)
    '================================ x86 patcher ======================
    Dim ini_patcher_x86 As New System.Threading.Thread(AddressOf ini_dl86)
    Dim client_patcher_x86 As New System.Threading.Thread(AddressOf client_dl86)
    Dim api_patcher_x86 As New System.Threading.Thread(AddressOf api_dl86)
    Dim finish_x86 As New System.Threading.Thread(AddressOf finished_x86)

    Public Sub patch64()
        ini_patcher_x64.Start()
        ini_patcher_x64.Join()      ' Wait for the thread to finish.
    End Sub

    Public Sub patch86()
        ini_patcher_x86.Start()
        ini_patcher_x86.Join()      ' Wait for the thread to finish.
    End Sub
#Region "Patcher x64"
    Public Sub ini_dl64()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/LumaEmu.ini"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/LumaEmu.ini", FILE_NAME)
                client_patcher_x64.Start()
                client_patcher_x64.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/LumaEmu.ini", FILE_NAME)
                client_patcher_x64.Start()
                client_patcher_x64.Join()
            End If
            End
        Catch ex As Exception
        End Try
    End Sub

    Public Sub client_dl64()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/steam_api64.dll"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/steam_api64.dll", FILE_NAME)
                api_patcher_x64.Start()
                api_patcher_x64.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/steam_api64.dll", FILE_NAME)
                api_patcher_x64.Start()
                api_patcher_x64.Join()
            End If
            End
        Catch ex As Exception
        End Try
    End Sub

    Public Sub api_dl64()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/steamclient64.dll"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/steamclient64.dll", FILE_NAME)
                finish_x64.Start()
                api_patcher_x64.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x64/steamclient64.dll", FILE_NAME)
                finish_x64.Start()
                api_patcher_x64.Join()
            End If
            End
            MsgBox("Patching is complete!")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub finished_x64()
        ini_patcher_x64.Abort()
        client_patcher_x64.Abort()
        api_patcher_x64.Abort()
        MsgBox("Pathing is complete!")
        LumaEmu.main()
    End Sub
#End Region

#Region "Patcher x86"

    Public Sub ini_dl86()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/LumaEmu.ini"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/LumaEmu.ini", FILE_NAME)
                client_patcher_x86.Start()
                client_patcher_x86.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/LumaEmu.ini", FILE_NAME)
                client_patcher_x86.Start()
                client_patcher_x86.Join()
            End If
            End
        Catch ex As Exception
        End Try
    End Sub

    Public Sub client_dl86()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/steam_api.dll"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/steam_api.dll", FILE_NAME)
                api_patcher_x86.Start()
                api_patcher_x86.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/steam_api.dll", FILE_NAME)
                api_patcher_x86.Start()
                api_patcher_x86.Join()
            End If
            End
        Catch ex As Exception
        End Try
    End Sub

    Public Sub api_dl86()
        Try
            Dim FILE_NAME As String = My.Settings.client_path & "/steamclient.dll"
            If System.IO.File.Exists(FILE_NAME) = False Then
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/steamclient.dll", FILE_NAME)
                finish_x86.Start()
                api_patcher_x86.Join()
            Else
                My.Computer.FileSystem.DeleteFile(FILE_NAME)
                My.Computer.Network.DownloadFile("http://hexhunterz.me.pn/client/x86/steamclient.dll", FILE_NAME)
                finish_x86.Start()
                api_patcher_x86.Join()
            End If
            End
            MsgBox("Patching is complete!")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub finished_x86()
        ini_patcher_x86.Abort()
        client_patcher_x86.Abort()
        api_patcher_x86.Abort()
        MsgBox("Pathing is complete!")
        LumaEmu.main()
    End Sub

#End Region

End Module
