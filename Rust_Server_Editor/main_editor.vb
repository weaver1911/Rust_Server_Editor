﻿Imports System.IO
Imports System.Threading
Imports System.Net
Imports System.Reflection
Imports Microsoft.Win32
Imports System.Net.NetworkInformation
Imports System.Net.NetworkInformation.Ping
Imports System.Collections
Imports System.Xml
Imports System.Runtime.InteropServices

Public Class Main_Editor
    'Rust Seeds: min = 0   Max = 4294967295
    Public server As String
    Public srv_port As Integer
    Public srv_hostname As String
    Public srv_players As Integer
    Public srv_secure As Integer
    Public srv_Identity As String
    Public rcon_port As Integer
    Public rcon_pass As String
    Public srv_chat As Integer
    Public srv_seed As String
    Public srv_worldsize As Integer
    Public srv_update As String
    Public srv_save As Integer
    Public Batch_Script As String
    ' Threading for write and print data
    Dim store_data_thread As New Threading.Thread(AddressOf Store_Data)
    Dim write_data_thread As New Threading.Thread(AddressOf Write_Data)


    Private Sub main_editor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Control.CheckForIllegalCrossThreadCalls = False
        If Not My.Settings.client_path = Nothing Xor My.Settings.server_path = Nothing Then
            settings.Show()
        End If
    End Sub

    Private Sub start_srv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles start_srv.Click
        store_data_thread.Start()
    End Sub

    Private Sub seed_gen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles seed_gen.Click
        Try
            seed_generator.RunWorkerAsync()
        Catch ex As Exception
            seed_gen.Enabled = False
        End Try
    End Sub

    '''''''''''''''''''''''' Check states '''''''''''''''''''''
#Region "integers only"

    Private Sub txt_port_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_port.KeyPress
        '97 - 122 = Ascii codes for simple letters
        '65 - 90  = Ascii codes for capital letters
        '48 - 57  = Ascii codes for numbers
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_world_size_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_world_size.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txt_seed_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_seed.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub save_int_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles save_int.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

#End Region
#Region "Check states"
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles check_secure.CheckedChanged
        If check_secure.Checked Then
            check_secure.Text = "True"
            srv_secure = 1
            check_secure.ForeColor = Color.DarkGreen
        Else : check_secure.Text = "False"
            srv_secure = 0
            check_secure.ForeColor = Color.DarkRed
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_chat.CheckedChanged
        If Check_chat.Checked Then
            Check_chat.Text = "True"
            srv_chat = 1
            Check_chat.ForeColor = Color.DarkGreen
        Else : Check_chat.Text = "False"
            Check_chat.ForeColor = Color.DarkRed
            srv_chat = 0
        End If
    End Sub

    Private Sub server_Update_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles server_Update.CheckedChanged
        If server_Update.Checked Then
            server_Update.Text = "True"
            srv_update = " -autoupdate"
            server_Update.ForeColor = Color.DarkGreen
        Else : server_Update.Text = "False"
            server_Update.ForeColor = Color.DarkRed
            srv_update = ""
        End If
    End Sub


#End Region
    ''''''''''''''''''' End state. Results'''''''''''''''''''''

    '[START] Store and write data with the press of "Write script: button
#Region "strore/write data"
    Public Sub Store_Data()
        srv_hostname = txt_hostname.Text
        srv_port = txt_port.Text
        srv_players = txt_players_max.Value
        srv_Identity = txt_Identity.Text
        rcon_port = txt_rcon_port.Text
        rcon_pass = txt_rcon_pass.Text
        srv_seed = txt_seed.Text
        srv_worldsize = txt_world_size.Text
        srv_save = save_int.Text
        Batch_Script = "RustDedicated.exe -batchmode" & " +server.hostname " & srv_hostname & " +server.port " & srv_port & " +server.maxplayers " & srv_players & " +server.secure " & srv_secure & " +server.identity " & srv_Identity & " +rcon.port " & rcon_port & " +rcon.password " & rcon_pass & " +server.globalchat " & srv_chat & " +server.seed " & srv_seed & " +server.worldsize " & srv_worldsize & " +server.saveinterval " & srv_save & srv_update
        write_data_thread.Start()
    End Sub

    Public Sub Write_Data()
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''' Batch script maker ''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Try
            Dim FILE_NAME As String = My.Settings.server_path & "\!start_server.bat"

            If System.IO.File.Exists(FILE_NAME) = False Then
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME)
                objWriter.Write(Batch_Script) '<------ Save input
                objWriter.Close()
            Else
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME)
                objWriter.Write(Batch_Script) '<------ Save input
                objWriter.Close()
            End If
            MsgBox("Batch script has been created. Program will now exit", MsgBoxStyle.Information)
            End
        Catch ex As Exception
            MsgBox("Oops. Unknown error!")
        End Try
    End Sub

#End Region
    '[END] Store and write data with the press of "Write script: button

    '[START] Save and Load RSE
#Region "New/Save/Load RSE"

    Private Sub NewScriptToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewScriptToolStripMenuItem.Click
        txt_hostname.Clear()
        txt_Identity.Clear()
        txt_rcon_pass.Clear()
        txt_seed.Clear()
        txt_world_size.Text = 4000
        txt_port.Text = 28015
        txt_players_max.Value = 200
        txt_rcon_port.Text = 28016
    End Sub

    Private Sub SaveScriptToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveScriptToolStripMenuItem.Click
        Dim myStream As Stream
        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.InitialDirectory = My.Settings.server_path
        saveFileDialog1.Filter = "RSE files (*.RSE)|*.RSE"
        saveFileDialog1.FilterIndex = 2
        saveFileDialog1.RestoreDirectory = True

        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
            myStream = saveFileDialog1.OpenFile()
            If (myStream IsNot Nothing) Then

                Dim settings As New XmlWriterSettings()
                settings.Indent = True
                Dim RseWrt As XmlWriter = XmlWriter.Create(myStream, settings)

                With RseWrt
                    ' Write the Xml declaration.
                    .WriteStartDocument()
                    ' Write a comment.
                    .WriteComment("This is Rust Server Editor (RSE) data")
                    ' Write the root element.
                    .WriteStartElement("Data")
                    ' Start our first person.
                    .WriteStartElement("Server")
                    ' The person nodes.
                    .WriteStartElement("hostname")
                    .WriteString(txt_hostname.Text)
                    .WriteEndElement()
                    .WriteStartElement("Port")
                    .WriteString(txt_port.Text)
                    .WriteEndElement()
                    .WriteStartElement("Max_Players")
                    .WriteString(txt_players_max.Text)
                    .WriteEndElement()
                    .WriteStartElement("Identity")
                    .WriteString(txt_Identity.Text)
                    .WriteEndElement()
                    .WriteStartElement("Rcon_Port")
                    .WriteString(txt_rcon_port.Text)
                    .WriteEndElement()
                    .WriteStartElement("Rcon_Pass")
                    .WriteString(txt_rcon_pass.Text)
                    .WriteEndElement()
                    .WriteStartElement("Seed")
                    .WriteString(txt_seed.Text)
                    .WriteEndElement()
                    .WriteStartElement("Worldsize")
                    .WriteString(txt_world_size.Text)
                    .WriteEndElement()
                    .WriteStartElement("Secure")
                    .WriteString(check_secure.Text)
                    .WriteEndElement()
                    .WriteStartElement("Chat")
                    .WriteString(Check_chat.Text)
                    .WriteEndElement()
                    .WriteStartElement("Update")
                    .WriteString(server_Update.Text)
                    .WriteEndElement()
                    .WriteStartElement("Save_Interval")
                    .WriteString(save_int.Text)
                    .WriteEndElement()
                    ' The end of this person.
                    .WriteEndElement()
                    ' Close the XmlTextWriter.
                    .WriteEndDocument()
                    .Close()
                End With
                MessageBox.Show("RSE file saved.")
                myStream.Close()
            End If
        End If
    End Sub

    Private Sub LoadScriptToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadScriptToolStripMenuItem.Click
        Dim myStream As Stream = Nothing
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = My.Settings.server_path
        openFileDialog1.Filter = "RSE files (*.RSE)|*.RSE"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                myStream = openFileDialog1.OpenFile()
                If (myStream IsNot Nothing) Then
                    'create a new xmltextreader object
                    'this is the object that we will loop and will be used to read the xml file

                    Dim RSE_document As XmlReader = New XmlTextReader(myStream)
                    'loop through the xml file
                    While (RSE_document.Read())

                        Dim type = RSE_document.NodeType

                        'if node type was element
                        If (type = XmlNodeType.Element) Then

                            'if the loop found a <FirstName> tag
                            If (RSE_document.Name = "hostname") Then
                                txt_hostname.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Port") Then
                                txt_port.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            If (RSE_document.Name = "Max_Players") Then
                                txt_players_max.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Identity") Then
                                txt_Identity.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            If (RSE_document.Name = "Rcon_Port") Then
                                txt_rcon_port.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Rcon_Pass") Then
                                txt_rcon_pass.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            If (RSE_document.Name = "Seed") Then
                                txt_seed.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Worldsize") Then
                                txt_world_size.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            If (RSE_document.Name = "Secure") Then
                                check_secure.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Chat") Then
                                Check_chat.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            If (RSE_document.Name = "Update") Then
                                server_Update.Text = RSE_document.ReadInnerXml.ToString()
                            End If

                            'if the loop found a <LastName tag
                            If (RSE_document.Name = "Save_interval") Then
                                save_int.Text = RSE_document.ReadInnerXml.ToString()
                            End If
                        End If

                    End While
                End If
            Catch Ex As Exception
                ' MessageBox.Show("Cannot read file from disk. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open. 
                If (myStream IsNot Nothing) Then
                    myStream.Close()
                End If
            End Try
        End If
    End Sub
#End Region
    '[END]   Save and Load RSE

    Private Sub main_editor_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            My.Settings.Save()
            End
        Catch ex As Exception
            End
        End Try
    End Sub

    Function SeedGEN() As Long
        Dim seed As String = ""
        Dim min_seed As Integer = 1
        Dim max_seed As Long = 4294967295
        ' Create the Random seed:
        Dim possible As String = "0123456789"
        For i As Integer = 0 To 9
            seed += possible.Chars(CInt(Math.Floor(Rnd() * possible.Length) Mod possible.Length))
        Next
        srv_seed = seed
        Return Convert.ToInt64(seed)
    End Function

    Private Sub seed_generator_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles seed_generator.DoWork
        SeedGEN()
        Seed_Status.Text = "Generating seed..."
        Thread.Sleep(250)
        txt_seed.Text = srv_seed
    End Sub

    Private Sub seed_generator_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles seed_generator.RunWorkerCompleted
        Try
            If srv_seed > 4294967295 Or srv_seed.StartsWith("0") Then
                seed_generator.RunWorkerAsync()
            End If
            Seed_Status.Text = "Success: Seed Accepted"
        Catch ex As Exception
            Seed_Status.Text = ErrorToString()
        End Try

    End Sub

    Private Sub Changelog_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
        Try
            Dim address As String = "http://hexhunterz.me.pn/changelog.txt"
            Dim client As WebClient = New WebClient()
            Dim reader As StreamReader = New StreamReader(client.OpenRead(address))
            Dim line As String = reader.ReadLine
            Dim list As New List(Of String)

            Do While (True)
                line = reader.ReadLine
                TextBox2.AppendText(Environment.NewLine & line)
                If line.StartsWith("#") Then
                    Exit Do
                End If
            Loop
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GamePathsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GamePathsToolStripMenuItem.Click
        settings.Show()
    End Sub

    Private Sub Luma_create_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        LumaEmu.main()
    End Sub

    Private Sub patch_btn64_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles patch_btn64.Click
        player_name_Luma = Luma_Name.Text
        Downloader.patch64()
    End Sub

    Private Sub patch_btn86_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles patch_btn86.Click
        player_name_Luma = Luma_Name.Text
        Downloader.patch86()
    End Sub

    Private Sub Privilege_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Privilege_btn.Click
        privilege.Show()
    End Sub

    Private Sub Downloader_bgw_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles Downloader_bgw.DoWork

    End Sub

    Private Sub ReleasePageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReleasePageToolStripMenuItem.Click
        Process.Start("https://github.com/OffensiveCoders/Rust_Server_Editor/releases")
    End Sub

    Private Sub SourceCodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SourceCodeToolStripMenuItem.Click
        Process.Start("https://github.com/OffensiveCoders/Rust_Server_Editor")
    End Sub
End Class

Public Class Win32
    <DllImport("kernel32.dll")> Public Shared Function AllocConsole() As Boolean
    End Function
    <DllImport("kernel32.dll")> Public Shared Function FreeConsole() As Boolean
    End Function
End Class
