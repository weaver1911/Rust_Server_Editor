﻿Module LumaEmu
    Public player_name_Luma As String
    Sub main()
        Dim Luma_Complete As String
        Dim LumaPath As String = My.Settings.client_path
        Dim LumaEmu1 As String = _
"[Player]" & vbNewLine & _
"PlayerName = " & player_name_Luma & vbNewLine & _
"PlayerNickname = " & player_name_Luma & vbNewLine & _
"ClanName = " & player_name_Luma & vbNewLine & _
"ClanTag = " & player_name_Luma & vbNewLine & _
    "[Language]" & vbNewLine & _
    "GameLanguage = english" & vbNewLine & _
    vbNewLine & _
    "[Cache]" & vbNewLine & _
    "# This will enable loading apps from GCF files" & vbNewLine & _
    "UseCacheFiles = 0" & vbNewLine & _
    vbNewLine & _
    "# Full path to the Steamapps folder, there must be an backslash at the end of the path." & vbNewLine & _
    "CachePath = C:\Program Files (x86)\Steam\steamapps\" & vbNewLine & _
    "[Log]" & vbNewLine & _
    "# Create LumaEmu.log and LumaEmu_Steamclient.log" & vbNewLine & _
    "LogFile = 1" & vbNewLine & _
    vbNewLine & _
    "# Log all the players on the server" & vbNewLine & _
    "LogPlayers = 0" & vbNewLine & _
    vbNewLine & _
    "[MasterServer]" & vbNewLine & _
    "# Set this to 1 to use Valve master server or set it to 2 to use setti master server, this setting is only used by Steam.dll." & vbNewLine & _
    "Master = 1" & vbNewLine & _
    vbNewLine & _
    "[DLC]" & vbNewLine & _
    "# With this you can enable and disable DLCs in games" & vbNewLine & _
    "UnlockDLC = 1" & vbNewLine & _
    vbNewLine & _
    "# Use this if you see the line ""BGetDLCDataByIndex"" in the log file. You can use multiple AppID's by calling each line AppID_1, AppID_2 and so on." & vbNewLine & _
    "AppID_0 = " & vbNewLine & _
    vbNewLine & _
    "[Overlay]" & vbNewLine & _
    "# This will tell the game if the Steam Overlay is available" & vbNewLine & _
    "EnableOverlay = 1" & vbNewLine & _
    vbNewLine & _
    "[StatsAndAchievements]" & vbNewLine & _
    "# Save Stats and Achievements" & vbNewLine & _
    "# 1 will save both, 2 will save achievements and 3 will save stats" & vbNewLine & _
    "Save = 1" & vbNewLine & _
    vbNewLine & _
    "[SourceEngine]" & vbNewLine & _
    "# With this enabled you will not lose FPS when the game window does not have focus, only works with Source Engine games." & vbNewLine & _
    "FocusPatch = 1" & vbNewLine & _
    vbNewLine & _
    "[Steam2Wrapper]" & vbNewLine & _
    "# Used by Steam2WrapperLauncher.exe and Steam2WrapperLauncher_x64.exe" & vbNewLine & _
    "GameExe = rust.exe - appid" & vbNewLine & _
    vbNewLine & _
    "[ServerAuthorization]" & vbNewLine & _
    "BlockOldLumaEmu = 1" & vbNewLine & _
    "BlockLegitSteam = 0" & vbNewLine & _
    "BlockGreenLuma = 0" & vbNewLine & _
    "BlockSmartSteamEmu = 1" & vbNewLine & _
    "BlockRevEmu = 1" & vbNewLine & _
    vbNewLine & _
    "[VR]" & vbNewLine & _
    "# This will tell games that Steam is running in VR mode." & vbNewLine & _
    "EnableVR = 1" & vbNewLine & _
    vbNewLine & _
    "[SteamClient]" & vbNewLine & _
    "# Set path to steamclient.dll or steamclient64.dll (not the original)" & vbNewLine & _
    "SteamClientDll = steamclient.dll"
        '---------------------Declaration of Luma Paths-----------------------------
        Luma_Complete = LumaEmu1


        '--------------------- Save Current Settings (if the file is already exists------------------------
        If My.Computer.FileSystem.FileExists(My.Settings.client_path & "/LumaEmu.ini") Then
            My.Computer.FileSystem.DeleteFile(My.Settings.client_path & "/LumaEmu.ini")
            Try
                My.Computer.FileSystem.WriteAllText(My.Settings.client_path & "/LumaEmu.ini", Luma_Complete, True)
            Catch ex As Exception
                MsgBox("You have no rights to write files here." & vbNewLine & _
                       "Try using the app as administrator, or change the directory path", MsgBoxStyle.Critical)
            End Try
            MsgBox("LumaEmu.ini successfully Re-created on:" & vbNewLine & _
                   LumaPath & vbNewLine & _
                   "with the following details:" & vbNewLine & _
                   "PlayerName = " & player_name_Luma & vbNewLine & _
                   "PlayerNickname = " & player_name_Luma & vbNewLine & _
                   "ClanName = " & player_name_Luma & vbNewLine & _
                   "ClanTag = " & player_name_Luma & vbNewLine & _
                   vbNewLine & _
                   "Press 'OK' to exit the wizard", MsgBoxStyle.OkOnly)
        Else
            '--------------------- Save Current Settings ------------------------
            Try
                My.Computer.FileSystem.WriteAllText(My.Settings.client_path & "/LumaEmu.ini", Luma_Complete, True)
            Catch ex As Exception
                MsgBox("You have no rights to write files here." & vbNewLine & _
                       "Try using the app as administrator, or change the directory path", MsgBoxStyle.Critical)
            End Try
            MsgBox("LumaEmu.ini successfully created on:" & vbNewLine & _
                   LumaPath & vbNewLine & _
                   "with the following details:" & vbNewLine & _
                   "PlayerName = " & player_name_Luma & vbNewLine & _
                   "PlayerNickname = " & player_name_Luma & vbNewLine & _
                   "ClanName = " & player_name_Luma & vbNewLine & _
                   "ClanTag = " & player_name_Luma & vbNewLine & _
                    vbNewLine & _
                    "Press 'OK' to exit the wizard", MsgBoxStyle.OkOnly)
        End If
    End Sub
End Module
