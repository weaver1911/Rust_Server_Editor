﻿Module downloader_v2
    Private Worker As System.Threading.Thread
    Private Warlock As New Object
    Private MyQueue As New Queue(Of Object)
    Private QuitWork As Boolean
    Private DoWork As New System.Threading.EventWaitHandle(False, Threading.EventResetMode.ManualReset)

    Public Sub Terminate()
        ' Terminates the worker thread 
        QuitWork = True
        DoWork.Set()
        Worker.Join(1000)
    End Sub

    Public Sub Start()
        ' start the worker thread 
        Worker = New Threading.Thread(AddressOf MyThread)
        Worker.IsBackground = True
        Worker.Start()
    End Sub

    Public Sub AddObject(ByVal obj As Object)
        ' Add an object to the queue 
        ' Always access the QUEUE in a synclock block 
        SyncLock Warlock
            MyQueue.Enqueue(obj)
        End SyncLock
        DoWork.Set()
    End Sub

    Public Sub MyThread()
        Debug.WriteLine("Thread started")
        Do
            Debug.WriteLine("Thread Waiting...")
            DoWork.WaitOne(-1, False)
            Debug.WriteLine("...Work to do?")
            If QuitWork Then Exit Do
            Dim dequeuedObject As Object
            Do
                ' 
                dequeuedObject = Nothing
                Debug.WriteLine("...Dequeue...")
                ' Always access the QUEUE in a synclock block 
                SyncLock Warlock
                    If MyQueue.Count > 0 Then
                        dequeuedObject = MyQueue.Dequeue
                    End If
                End SyncLock
                ' 
                If dequeuedObject IsNot Nothing Then
                    Debug.WriteLine("...Working...")
                    ' Simulate work 
                    Threading.Thread.Sleep(2000)
                    Debug.WriteLine("...Work Complete.")
                End If
                ' 
            Loop While dequeuedObject IsNot Nothing
            ' 
            ' Always access the QUEUE in a synclock block 
            SyncLock Warlock
                If MyQueue.Count = 0 Then DoWork.Reset()
            End SyncLock

        Loop
        ' 
        Debug.WriteLine("THREAD ENDED")
    End Sub

End Module
