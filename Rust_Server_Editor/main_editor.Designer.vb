﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_Editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Editor))
        Me.helpdesk = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewScriptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadScriptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveScriptToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GamePathsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReleasePageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SourceCodeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.seed_generator = New System.ComponentModel.BackgroundWorker()
        Me.StatusBar = New System.Windows.Forms.StatusStrip()
        Me.Seed_Status = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Privilege_btn = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.start_srv = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.server_Update = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.check_secure = New System.Windows.Forms.CheckBox()
        Me.Check_chat = New System.Windows.Forms.CheckBox()
        Me.save_int = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txt_rcon_port = New System.Windows.Forms.TextBox()
        Me.txt_players_max = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_hostname = New System.Windows.Forms.TextBox()
        Me.txt_port = New System.Windows.Forms.TextBox()
        Me.txt_Identity = New System.Windows.Forms.TextBox()
        Me.txt_rcon_pass = New System.Windows.Forms.TextBox()
        Me.txt_world_size = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.txt_seed = New System.Windows.Forms.TextBox()
        Me.seed_gen = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.patch_btn64 = New System.Windows.Forms.Button()
        Me.patch_btn86 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Luma_Name = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Downloader_bgw = New System.ComponentModel.BackgroundWorker()
        Me.MainMenu.SuspendLayout()
        Me.StatusBar.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.txt_players_max, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'helpdesk
        '
        Me.helpdesk.AutomaticDelay = 50
        Me.helpdesk.AutoPopDelay = 5000
        Me.helpdesk.InitialDelay = 50
        Me.helpdesk.ReshowDelay = 10
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MainMenu.Size = New System.Drawing.Size(477, 24)
        Me.MainMenu.TabIndex = 26
        Me.MainMenu.Text = "MainMenu"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewScriptToolStripMenuItem, Me.LoadScriptToolStripMenuItem, Me.SaveScriptToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'NewScriptToolStripMenuItem
        '
        Me.NewScriptToolStripMenuItem.Name = "NewScriptToolStripMenuItem"
        Me.NewScriptToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.NewScriptToolStripMenuItem.Text = "New script"
        '
        'LoadScriptToolStripMenuItem
        '
        Me.LoadScriptToolStripMenuItem.Name = "LoadScriptToolStripMenuItem"
        Me.LoadScriptToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.LoadScriptToolStripMenuItem.Text = "Load script"
        '
        'SaveScriptToolStripMenuItem
        '
        Me.SaveScriptToolStripMenuItem.Name = "SaveScriptToolStripMenuItem"
        Me.SaveScriptToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.SaveScriptToolStripMenuItem.Text = "Save Script"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GamePathsToolStripMenuItem})
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'GamePathsToolStripMenuItem
        '
        Me.GamePathsToolStripMenuItem.Name = "GamePathsToolStripMenuItem"
        Me.GamePathsToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.GamePathsToolStripMenuItem.Text = "Game paths"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReleasePageToolStripMenuItem, Me.SourceCodeToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.HelpToolStripMenuItem.Text = "GitHub page"
        '
        'ReleasePageToolStripMenuItem
        '
        Me.ReleasePageToolStripMenuItem.Name = "ReleasePageToolStripMenuItem"
        Me.ReleasePageToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ReleasePageToolStripMenuItem.Text = "Release page"
        '
        'SourceCodeToolStripMenuItem
        '
        Me.SourceCodeToolStripMenuItem.Name = "SourceCodeToolStripMenuItem"
        Me.SourceCodeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SourceCodeToolStripMenuItem.Text = "Source Code"
        '
        'seed_generator
        '
        '
        'StatusBar
        '
        Me.StatusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Seed_Status})
        Me.StatusBar.Location = New System.Drawing.Point(0, 326)
        Me.StatusBar.Name = "StatusBar"
        Me.StatusBar.Size = New System.Drawing.Size(477, 22)
        Me.StatusBar.SizingGrip = False
        Me.StatusBar.TabIndex = 25
        Me.StatusBar.Text = "StatusBar"
        '
        'Seed_Status
        '
        Me.Seed_Status.Name = "Seed_Status"
        Me.Seed_Status.Size = New System.Drawing.Size(26, 17)
        Me.Seed_Status.Text = "Idle"
        Me.Seed_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.HotTrack = True
        Me.TabControl1.Location = New System.Drawing.Point(0, 24)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(477, 302)
        Me.TabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.TabControl1.TabIndex = 27
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Privilege_btn)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.TableLayoutPanel3)
        Me.TabPage1.Controls.Add(Me.TableLayoutPanel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(469, 276)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Server"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Privilege_btn
        '
        Me.Privilege_btn.Location = New System.Drawing.Point(248, 173)
        Me.Privilege_btn.Name = "Privilege_btn"
        Me.Privilege_btn.Size = New System.Drawing.Size(213, 30)
        Me.Privilege_btn.TabIndex = 22
        Me.Privilege_btn.Text = "Privilege"
        Me.Privilege_btn.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.start_srv)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(248, 209)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(218, 64)
        Me.Panel1.TabIndex = 21
        '
        'start_srv
        '
        Me.start_srv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.start_srv.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.start_srv.Location = New System.Drawing.Point(0, 0)
        Me.start_srv.Name = "start_srv"
        Me.start_srv.Size = New System.Drawing.Size(218, 64)
        Me.start_srv.TabIndex = 1
        Me.start_srv.TabStop = False
        Me.start_srv.Text = "Write Script"
        Me.start_srv.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.Controls.Add(Me.Label11, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.server_Update, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label12, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label8, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.check_secure, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Check_chat, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.save_int, 1, 3)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(248, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(218, 132)
        Me.TableLayoutPanel3.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label11.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(5, 98)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(158, 32)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Save (sec's)"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'server_Update
        '
        Me.server_Update.Appearance = System.Windows.Forms.Appearance.Button
        Me.server_Update.AutoSize = True
        Me.server_Update.Dock = System.Windows.Forms.DockStyle.Fill
        Me.server_Update.Enabled = False
        Me.server_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.server_Update.ForeColor = System.Drawing.Color.DarkRed
        Me.server_Update.Location = New System.Drawing.Point(171, 69)
        Me.server_Update.Name = "server_Update"
        Me.server_Update.Size = New System.Drawing.Size(42, 24)
        Me.server_Update.TabIndex = 51
        Me.server_Update.Text = "False"
        Me.server_Update.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.server_Update.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label12.Enabled = False
        Me.Label12.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 66)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(158, 30)
        Me.Label12.TabIndex = 52
        Me.Label12.Text = "Auto Update"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label4.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 30)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Secure"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label8.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(5, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(158, 30)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Global chat"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'check_secure
        '
        Me.check_secure.Appearance = System.Windows.Forms.Appearance.Button
        Me.check_secure.AutoSize = True
        Me.check_secure.Dock = System.Windows.Forms.DockStyle.Fill
        Me.check_secure.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.check_secure.ForeColor = System.Drawing.Color.DarkRed
        Me.check_secure.Location = New System.Drawing.Point(171, 5)
        Me.check_secure.Name = "check_secure"
        Me.check_secure.Size = New System.Drawing.Size(42, 24)
        Me.check_secure.TabIndex = 4
        Me.check_secure.Text = "False"
        Me.check_secure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.check_secure.UseVisualStyleBackColor = True
        '
        'Check_chat
        '
        Me.Check_chat.Appearance = System.Windows.Forms.Appearance.Button
        Me.Check_chat.AutoSize = True
        Me.Check_chat.Checked = True
        Me.Check_chat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Check_chat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Check_chat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Check_chat.ForeColor = System.Drawing.Color.DarkGreen
        Me.Check_chat.Location = New System.Drawing.Point(171, 37)
        Me.Check_chat.Name = "Check_chat"
        Me.Check_chat.Size = New System.Drawing.Size(42, 24)
        Me.Check_chat.TabIndex = 8
        Me.Check_chat.Text = "True"
        Me.Check_chat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Check_chat.UseVisualStyleBackColor = True
        '
        'save_int
        '
        Me.save_int.Dock = System.Windows.Forms.DockStyle.Fill
        Me.save_int.Location = New System.Drawing.Point(171, 101)
        Me.save_int.Name = "save_int"
        Me.save_int.Size = New System.Drawing.Size(42, 20)
        Me.save_int.TabIndex = 54
        Me.save_int.Text = "3600"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.73663!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.26337!))
        Me.TableLayoutPanel1.Controls.Add(Me.txt_rcon_port, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_players_max, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_hostname, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_port, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_Identity, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_rcon_pass, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txt_world_size, 1, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 6)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(245, 270)
        Me.TableLayoutPanel1.TabIndex = 12
        '
        'txt_rcon_port
        '
        Me.txt_rcon_port.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_rcon_port.Location = New System.Drawing.Point(121, 137)
        Me.txt_rcon_port.MaxLength = 99999
        Me.txt_rcon_port.Name = "txt_rcon_port"
        Me.txt_rcon_port.Size = New System.Drawing.Size(119, 20)
        Me.txt_rcon_port.TabIndex = 6
        Me.txt_rcon_port.Text = "28016"
        '
        'txt_players_max
        '
        Me.txt_players_max.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_players_max.Location = New System.Drawing.Point(121, 71)
        Me.txt_players_max.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.txt_players_max.Name = "txt_players_max"
        Me.txt_players_max.Size = New System.Drawing.Size(119, 20)
        Me.txt_players_max.TabIndex = 3
        Me.txt_players_max.Value = New Decimal(New Integer() {200, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 31)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Hostname"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label10.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(5, 233)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(108, 35)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "World size"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 31)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Port"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label9.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 200)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 31)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Seed"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 31)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Max Players"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label7.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 167)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(108, 31)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "rcon password"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label5.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 31)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Identity"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label6.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 31)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "rcon port"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_hostname
        '
        Me.txt_hostname.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_hostname.Location = New System.Drawing.Point(121, 5)
        Me.txt_hostname.Name = "txt_hostname"
        Me.txt_hostname.Size = New System.Drawing.Size(119, 20)
        Me.txt_hostname.TabIndex = 1
        '
        'txt_port
        '
        Me.txt_port.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_port.Location = New System.Drawing.Point(121, 38)
        Me.txt_port.MaxLength = 99999
        Me.txt_port.Name = "txt_port"
        Me.txt_port.Size = New System.Drawing.Size(119, 20)
        Me.txt_port.TabIndex = 2
        Me.txt_port.Text = "28015"
        '
        'txt_Identity
        '
        Me.txt_Identity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_Identity.Location = New System.Drawing.Point(121, 104)
        Me.txt_Identity.Name = "txt_Identity"
        Me.txt_Identity.Size = New System.Drawing.Size(119, 20)
        Me.txt_Identity.TabIndex = 5
        '
        'txt_rcon_pass
        '
        Me.txt_rcon_pass.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_rcon_pass.Location = New System.Drawing.Point(121, 170)
        Me.txt_rcon_pass.Name = "txt_rcon_pass"
        Me.txt_rcon_pass.Size = New System.Drawing.Size(119, 20)
        Me.txt_rcon_pass.TabIndex = 7
        '
        'txt_world_size
        '
        Me.txt_world_size.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_world_size.Location = New System.Drawing.Point(121, 236)
        Me.txt_world_size.Name = "txt_world_size"
        Me.txt_world_size.Size = New System.Drawing.Size(119, 20)
        Me.txt_world_size.TabIndex = 50
        Me.txt_world_size.Text = "4000"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.86555!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.13445!))
        Me.TableLayoutPanel2.Controls.Add(Me.txt_seed, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.seed_gen, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(121, 203)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(119, 25)
        Me.TableLayoutPanel2.TabIndex = 26
        '
        'txt_seed
        '
        Me.txt_seed.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txt_seed.Location = New System.Drawing.Point(3, 3)
        Me.txt_seed.Name = "txt_seed"
        Me.txt_seed.Size = New System.Drawing.Size(70, 20)
        Me.txt_seed.TabIndex = 9
        '
        'seed_gen
        '
        Me.seed_gen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.seed_gen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.seed_gen.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.seed_gen.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seed_gen.Location = New System.Drawing.Point(79, 3)
        Me.seed_gen.Name = "seed_gen"
        Me.seed_gen.Size = New System.Drawing.Size(37, 19)
        Me.seed_gen.TabIndex = 21
        Me.seed_gen.TabStop = False
        Me.seed_gen.Text = "Rnd"
        Me.seed_gen.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.seed_gen.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.Transparent
        Me.TabPage2.Controls.Add(Me.TableLayoutPanel4)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Luma_Name)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(469, 276)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Client"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.patch_btn64, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.patch_btn86, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(148, 196)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(173, 56)
        Me.TableLayoutPanel4.TabIndex = 15
        '
        'patch_btn64
        '
        Me.patch_btn64.Dock = System.Windows.Forms.DockStyle.Fill
        Me.patch_btn64.Location = New System.Drawing.Point(3, 3)
        Me.patch_btn64.Name = "patch_btn64"
        Me.patch_btn64.Size = New System.Drawing.Size(80, 50)
        Me.patch_btn64.TabIndex = 12
        Me.patch_btn64.Text = "Patch Client(x64)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.patch_btn64.UseVisualStyleBackColor = True
        '
        'patch_btn86
        '
        Me.patch_btn86.Dock = System.Windows.Forms.DockStyle.Fill
        Me.patch_btn86.Location = New System.Drawing.Point(89, 3)
        Me.patch_btn86.Name = "patch_btn86"
        Me.patch_btn86.Size = New System.Drawing.Size(81, 50)
        Me.patch_btn86.TabIndex = 14
        Me.patch_btn86.Text = "Patch Client(x86)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.patch_btn86.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(31, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(407, 91)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = resources.GetString("Label13.Text")
        '
        'Luma_Name
        '
        Me.Luma_Name.Location = New System.Drawing.Point(148, 46)
        Me.Luma_Name.Name = "Luma_Name"
        Me.Luma_Name.Size = New System.Drawing.Size(173, 20)
        Me.Luma_Name.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(201, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(67, 13)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "Player Name"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.TextBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(469, 276)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Changelog / HELP"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox2.Location = New System.Drawing.Point(0, 0)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox2.Size = New System.Drawing.Size(469, 276)
        Me.TextBox2.TabIndex = 0
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        Me.TextBox2.Visible = False
        '
        'Downloader_bgw
        '
        '
        'Main_Editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(477, 348)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MainMenu)
        Me.Controls.Add(Me.StatusBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Main_Editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RUST | Server Editor  [v1.5]"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.StatusBar.ResumeLayout(False)
        Me.StatusBar.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.txt_players_max, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents helpdesk As System.Windows.Forms.ToolTip
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewScriptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadScriptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveScriptToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GamePathsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents seed_generator As System.ComponentModel.BackgroundWorker
    Friend WithEvents StatusBar As System.Windows.Forms.StatusStrip
    Friend WithEvents Seed_Status As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents start_srv As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents server_Update As System.Windows.Forms.CheckBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents check_secure As System.Windows.Forms.CheckBox
    Friend WithEvents Check_chat As System.Windows.Forms.CheckBox
    Friend WithEvents save_int As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txt_rcon_port As System.Windows.Forms.TextBox
    Friend WithEvents txt_players_max As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_hostname As System.Windows.Forms.TextBox
    Friend WithEvents txt_port As System.Windows.Forms.TextBox
    Friend WithEvents txt_Identity As System.Windows.Forms.TextBox
    Friend WithEvents txt_rcon_pass As System.Windows.Forms.TextBox
    Friend WithEvents txt_world_size As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txt_seed As System.Windows.Forms.TextBox
    Friend WithEvents seed_gen As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Luma_Name As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents patch_btn64 As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents patch_btn86 As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Privilege_btn As System.Windows.Forms.Button
    Friend WithEvents Downloader_bgw As System.ComponentModel.BackgroundWorker
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReleasePageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SourceCodeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
